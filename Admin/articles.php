<?php 
   session_start();
// include init include to contect file and page navbar and style 
		    include 'init.php';
		    // page name
		  $pagetitle="Articles";
		//get to parameter do in link
		$do=isset($_GET['do'])?$_GET['do']:'Manage';
		// start manage Articles
				if($do === 'Manage')
		{
		?>
<!--  manage articles html-->
				<h1 class="text-center ">manage Articles</h1> 
				   <div class="container">
				   	<!--button Add new Article-->
				   	<a href="?do=ADD" class="btn btn-info ">ADD NEW Article</a>

				   <table class="table table-responsive ">
				   	<thead>
				   		<tr>
				   			<th>Number Of Article</th>
				   			<th>Title Article</th>
				   			<th>Content Article</th>
				   			<th>Image</th>
				   			<th>Publish  date of  article</th>
				   			<th>Control</th>
				   		</tr>
				   		
				   	</thead>
<?php 
				$statement=$con->prepare("SELECT * FROM articles");
				$statement->execute();
				$rows=$statement->fetchAll();
				  echo "<tbody>";
				 foreach ($rows as $key => $value) {
				echo "<tr>";
				echo "<td>".$value['id']."</td>";
				echo "<td>".$value['title']."</td>";
				echo "<td>".$value['content']."</td>";
				echo "<td><img src='uplodes/".$value['image']."' width='50' height='50'></td>";
				echo "<td>".$value['date']."</td>";
				echo "<td><a  href='?do=EDIT&&Editid=".$value['id']."' class='btn btn-success'>EDIT</a>
				<a  href='?do=DELETE&&Deleteid=".$value['id']."' class='btn btn-danger'>DELETE</a>
				</td>";
                echo "</tr>";

				 }
?>
			</tbody>
			</table>

</div>
<?php		
		}
// show ADD New articles
		else if($do==="ADD")
		{
       ?>
       <h1 class="text-center">Add New article</h1>
       <div class="container">
       <form method="POST" action="?do=Insert" class="control" enctype="multipart/form-data">
       	<div class="form-group ">
       		    <label for="inputTitle" class="col-sm-2 control-label">Title</label>
       		      <div class="col-sm-10">
      <input type="text" class="form-control form-group" id="inputTitle" name="Title">
    </div>
       	</div>
       	<div class="form-group ">
       		<label for="TEXTContent" class="col-sm-2 control-label">Content</label>
       		<div class="col-sm-10">
       			<textarea name="Content" class="form-control  form-group ckeditor" ></textarea>
       		</div>
       	</div>

  	<div class="form-group ">
       		<label for="Image" class="col-sm-2 control-label">Image</label>
       		<div class="col-sm-10">
      <input type="file" class="form-control form-group" id="Image" name="Image">
       		</div>
       	</div>
       	  	<div class="form-group">
       		    <label for="inputDate" class="col-sm-2 control-label">Date</label>
       		      <div class="col-sm-10">
      <input type="text" class="form-control  margin-top" id="inputDate" name="Date">
    </div>
       	</div>
       	  	  	<div class="form-group text-center">
       		    <label  class="col-sm-2"></label>

   <input type="submit" class="btn btn-info margin-top" value="Save">

       	</div>
       </form>
       </div>
       <?php

		}

		// Insert New articles
		else if ($do==="Insert")
		{
			// check  if method post
			if($_SERVER['REQUEST_METHOD']=="POST")
			{

           echo '<div class="container">';
           echo '<h1 class="text-center"> Insert Page</h1>';

				$Title=$_POST['Title'];
				$Content=$_POST['Content'];
				$Date=$_POST['Date'];
				//start image information
                

			// if (isset($_FILES['Image']) && $_FILES['Image']['error'] === UPLOAD_ERR_OK)
			//   {
			    // get details of the uploaded file
			    $fileTmpPath = $_FILES['Image']['tmp_name'];
			    $fileName = $_FILES['Image']['name'];
			    $fileSize = $_FILES['Image']['size'];
			    $fileType = $_FILES['Image']['type'];
			    $fileNameCmps = explode(".", $fileName);
			    $fileExtension = strtolower(end($fileNameCmps));
			 
			    // sanitize file-name
			    $newFileName = md5(time() . $fileName) . '.' . $fileExtension;
			 
			    // check if file has one of the following extensions
			    // $allowedfileExtensions = array('jpg', 'gif', 'png' /*'zip', 'txt', 'xls', 'doc'*/ );
						 
			    // if (in_array($fileExtension, $allowedfileExtensions))
			    // {
			      // directory in which the uploaded file will be moved
			      $uploadFileDir = 'uplodes/';
			      $dest_path = $uploadFileDir . $newFileName;
			 
			      // if(move_uploaded_file($fileTmpPath, $dest_path)) 
			      // {
						//use function to check name if exiset
			
		
    
					move_uploaded_file($fileTmpPath, $dest_path);
					//insert Information table  articles

					$statement=$con->prepare("INSERT INTO articles(title,content,date,image)
						VALUES(:mtitle,:mcontent,:mdate,:mimage)");
					$statement->execute( array(
						'mtitle'   => $Title,
						'mcontent' => $Content,
						'mdate'    => $Date,
						'mimage'   =>$newFileName
					 ));
	   $message='<div  class="alert alert-success"> '.$statement->rowCount().' Inserted'.'</div>';
	    Redurict($message,'back');     
			

			
				//       }
			 //   else
			 //      {
			 //        $message = 'There was some error moving the file to upload directory. Please make sure the upload directory is writable by web server.';
			  
			 //  		    Redurict($message,'back');     
    // }
			 //    }
			 //  else
			 //    {
			 //      $message = 'Upload failed. Allowed file types: ' . implode(',', $allowedfileExtensions);
			 //   		    Redurict($message,'back');     

			 //    }
			 //  }
			 //  else
			 //  {
			 //    $message = 'There is some error in the file upload. Please check the following error.<br>';
			 //    $message .= 'Error:' . $_FILES['Image']['error'];
			 //    		    Redurict($message,'back');     

			 //  }

				// //End Image Information 
			
				// }else{
				// // if method not POST
		  //   echo '<div class="container">';
		  //       $mas='<div  class="alert alert-danger">You Not Allow To Come Here </div> ';
		  //       Redurict($mas,'jjj');
				// }
			} 
		}
	//End Insert 

	//start EDIT
	else if($do=="EDIT"){
			 $Edit=isset($_GET['Editid']) && is_numeric($_GET['Editid'])? intval($_GET['Editid']):0;
			           $statement =$con->prepare("SELECT * 
			                                FROM 
			                                    articles
			                                WHERE 
			                                     id=? 
			                                   ");
			           $statement->execute(array($Edit));
			           $row=$statement->fetch();

			            $count=$statement->rowCount();

			            if($statement->rowCount() > 0){
		?>
       <h1 class="text-center">Edit New article</h1>
       <div class="container">
       <form method="POST" action="?do=Update" class="control" enctype="multipart/form-data">
       	<input type="hidden" name="Editid" value="<?php echo $row['id'] ?>">
       	<div class="form-group ">
       		    <label for="inputTitle" class="col-sm-2 control-label">Title</label>
       		      <div class="col-sm-10">
      <input type="text" class="form-control form-group" id="inputTitle" name="Title" value="<?php echo $row['title'] ?>">
    </div>
       	</div>
       	<div class="form-group ">
       		<label for="TEXTContent" class="col-sm-2 control-label">Content</label>
       		<div class="col-sm-10">
       			<textarea name="Content" class="form-control  form-group ckeditor" ><?php echo $row['content'] ; ?></textarea>
       		</div>
       	</div>
	<div class="form-group ">
       		<label for="Image" class="col-sm-2 control-label">Image</label>
       		<div class="col-sm-10">
      <input type="file" class="form-control form-group" id="Image" name="Image">
       		</div>
       	</div>
       
       	  	<div class="form-group">
       		    <label for="inputDate" class="col-sm-2 control-label">Date</label>
       		      <div class="col-sm-10">
      <input type="text" class="form-control  margin-top" id="inputDate" name="Date"value="<?php echo $row['date'] ?>">
    </div>
       	</div>
       	  	  	<div class="form-group text-center">
       		    <label  class="col-sm-2"></label>

   <input type="submit" class="btn btn-info margin-top" value="Save">

       	</div>
       </form>
		<?php
	}
	// if id edit not right
else{
 echo '<div class="container">';
        $mas='<div  class="alert alert-danger">You Edit id Not right </div> ';
        Redurict($mas,'jjj');
    }
}
	//End EDIT 

//start Update Information 
else if($do==="Update"){
		if($_SERVER['REQUEST_METHOD']=="POST")
				{

	           echo '<div class="container">';
	           echo '<h1 class="text-center"> Update  Page</h1>';
	           	$Title=$_POST['Title'];
				$Content=$_POST['Content'];
				$Date=$_POST['Date'];
				$editid=$_POST['Editid'];
	            $image=$_FILES['Image']['name'];
	            //if image exiset
	            if($image==''){
	            	$statement=$con->prepare("UPDATE articles SET title=?,Content=?,Date=? WHERE id=?");
	            	$statement->execute(array($Title,$Content,$Date,$editid));
	            	echo '<div class="container">';
	         $message='<div  class="alert alert-success"> '.$statement->rowCount().' Inserted'.'</div>';
	    Redurict($message,'back');    
	    } 
	            }else{
// if image not exiset
            	if(isset($image) && $_FILES['Image']['error'] === UPLOAD_ERR_OK){
			    $fileTmpPath = $_FILES['Image']['tmp_name'];
			    $fileName = $_FILES['Image']['name'];
			    $fileSize = $_FILES['Image']['size'];
			    $fileType = $_FILES['Image']['type'];
			    $fileNameCmps = explode(".", $fileName);
			    $fileExtension = strtolower(end($fileNameCmps));
			 
			    // sanitize file-name
			    $newFileName = md5(time() . $fileName) . '.' . $fileExtension;
			 
			    // check if file has one of the following extensions
			    $allowedfileExtensions = array('jpg', 'gif', 'png');
			 
			    if (in_array($fileExtension, $allowedfileExtensions))
			    {
			      // directory in which the uploaded file will be moved
			      $uploadFileDir = 'uplodes/';
			      $dest_path = $uploadFileDir . $newFileName;
			 
	      if(move_uploaded_file($fileTmpPath, $dest_path)) 
	      {
			
					//insert Information table  articles

					   



    					move_uploaded_file($fileTmpPath, $dest_path);

					$statement=$con->prepare("UPDATE articles SET title=?,Content=?,Date=?,Image=? WHERE id=?	");
	            	$statement->execute(array($Title,$Content,$Date,$newFileName,$editid));
	            	echo '<div class="container">';
	         $message='<div  class="alert alert-success"> '.$statement->rowCount().' Inserted'.'</div>';
	    Redurict($message,'back');     
				
				      }
				

				      else
				      {
				        $message = 'There was some error moving the file to upload directory. Please make sure the upload directory is writable by web server.';
				    Redurict($message,'back');     

				      }
	    }
	                else
                     {
		    $message = 'Upload failed. Allowed file types: ' . implode(',', $allowedfileExtensions);
              	    Redurict($message,'back');     

                     }
            	} 
            	else
                      {
		    $message = 'There is some error in the file upload. Please check the following error.<br>';
		    $message .= 'Error:' . $_FILES['Image']['error'];
               	    Redurict($message,'back');     

                       }

            }


}
//End Upate Information

//start delete
else if($do==='DELETE'){
	 $Edit=isset($_GET['Deleteid']) && is_numeric($_GET['Deleteid'])? intval($_GET['Deleteid']):0;
	   $checkItem=checkItem('id','articles',$Edit);
           if($checkItem > 0)
           {  
           	      $statement=$con->prepare("SELECT * FROM articles WHERE id=?");
                  $statement->execute(array($Edit));
                  $name=$statement->fetch();
                  $statement=$con->prepare("DELETE FROM articles WHERE id=?");
                  $statement->execute(array($Edit));
                  $filename="uplodes/".$name['image'];
				  if (file_exists($filename))
				  {
				  	       unlink($filename);
				  }
		
                  if($statement->rowCount() > 0)
                  {
                   ob_start();
                     	echo '<div class="container">';
                   $mas='<div class="alert alert-success">'.$statement->rowCount() .' Recored deleted'.'</div>';
                   Redurict($mas,'back');
                   }
          }

echo '</div>';
}
//end Delete

include "includes/templats/footer.php";