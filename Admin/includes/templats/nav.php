<nav class="navbar navbar-inverse">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-nav" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="dashbord.php"><?php echo lang("HOME-PAGE");   ?></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="app-nav">
      <ul class="nav navbar-nav">
                    <!-- articles page-->
          <li><a href="articles.php"><?php echo lang('articles') ?></a></li>
         <li><a href="book.php"><?php echo lang('BOOKS') ?></a></li>
         <li><a href="sound.php"><?php echo lang('audio') ?></a></li>
         <li><a href="lessons_and_lectures.php">lectures</a></li>
         <li><a href="research.php">research</a></li>
         <li><a href="alkhutab.php">alkhutab</a></li>
         <li><a href="consulting.php">consulting</a></li>
         <li><a href="translated_material.php">translated material</a></li>
         <li><a href="tweets.php">tweets</a></li>





      </ul>
      
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php  echo $_SESSION['username'];  ?> <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li>
            <a href="Member.php?do=Edit&userid=<?php echo $_SESSION['ID'] ?>"> <?php echo lang("Edit_PRO"); ?></a>

            </li>
            <li><a href="../../../"><?php echo lang("SETTING"); ?></a></li>

            <li><a href="logout.php"><?php echo lang("LOG_OUT"); ?></a></li>
    
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>