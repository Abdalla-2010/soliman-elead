<?php 
   session_start();
// include init include to contect file and page navbar and style 
		    include 'init.php';
		    // page name
		  $pagetitle="alkhutab";
		//get to parameter do in link
		$do=isset($_GET['do'])?$_GET['do']:'Manage';
		// start manage alkhutab
				if($do === 'Manage')
		{
		?>
<!--  manage alkhutab html-->
				<h1 class="text-center ">manage alkhutab</h1> 
				   <div class="container">
				   	<!--button Add new alkhutab-->
				   	<a href="?do=ADD" class="btn btn-info ">ADD NEW alkhutab</a>

				   <table class="table table-responsive ">
				   	<thead>
				   		<tr>
				   			<th>Number Of alkhutab</th>
				   			<th>Title alkhutab</th>
				   			<th>Content alkhutab</th>
				   			<th>date of  alkhutab</th>
				   			<th>Control</th>
				   		</tr>
				   		
				   	</thead>
<?php 
				$statement=$con->prepare("SELECT * FROM alkhutab");
				$statement->execute();
				$rows=$statement->fetchAll();
				  echo "<tbody>";
				 foreach ($rows as $key => $value) {
				echo "<tr>";
				echo "<td>".$value['id']."</td>";
				echo "<td>".$value['title']."</td>";
				echo "<td>".$value['content']."</td>";
				echo "<td>".$value['date']."</td>";
				echo "<td><a  href='?do=EDIT&&Editid=".$value['id']."' class='btn btn-success'>EDIT</a>
				<a  href='?do=DELETE&&Deleteid=".$value['id']."' class='btn btn-danger'>DELETE</a>
				</td>";
                echo "</tr>";

				 }
?>
			</tbody>
			</table>

</div>
<?php		
		}
// show ADD New alkhutab
		else if($do==="ADD")
		{
       ?>
       <h1 class="text-center">Add New alkhutab</h1>
       <div class="container">
       <form method="POST" action="?do=Insert" class="control" enctype="multipart/form-data">
       	<div class="form-group ">
       		    <label for="inputTitle" class="col-sm-2 control-label">Title</label>
       		      <div class="col-sm-10">
      <input type="text" class="form-control form-group" id="inputTitle" name="Title">
    </div>
       	</div>
       	<div class="form-group ">
       		<label for="TEXTContent" class="col-sm-2 control-label">Content</label>
       		<div class="col-sm-10">
       			<textarea name="Content" class="form-control  form-group ckeditor" ></textarea>
       		</div>
       	</div>

       	  	<div class="form-group">
       		    <label for="inputDate" class="col-sm-2 control-label">Date</label>
       		      <div class="col-sm-10">
      <input type="text" class="form-control  margin-top" id="inputDate" name="Date">
    </div>
       	</div>
       	  	  	<div class="form-group text-center">
       		    <label  class="col-sm-2"></label>

   <input type="submit" class="btn btn-info margin-top" value="Save">

       	</div>
       </form>
       </div>
       <?php

		}

		// Insert New alkhutab
		else if ($do==="Insert")
		{
			// check  if method post
			if($_SERVER['REQUEST_METHOD']=="POST")
			{

           echo '<div class="container">';
           echo '<h1 class="text-center"> Insert Page</h1>';

				$Title=$_POST['Title'];
				$Content=$_POST['Content'];
				$Date=$_POST['Date'];
	
      

			  
			
			    
						//use function to check name if exiset
			

					//insert Information table  alkhutab

					$statement=$con->prepare("INSERT INTO alkhutab(title,content,date)
						VALUES(:mtitle,:mcontent,:mdate)");
					$statement->execute( array(
						'mtitle'   => $Title,
						'mcontent' => $Content,
						'mdate'    => $Date
					 ));
	   $message='<div  class="alert alert-success"> '.$statement->rowCount().' Inserted'.'</div>';
	    Redurict($message,'back');     
			
		
		
			 
			
			
			} 
	//End Insert 
}	//start EDIT
	else if($do=="EDIT"){
			 $Edit=isset($_GET['Editid']) && is_numeric($_GET['Editid'])? intval($_GET['Editid']):0;
			           $statement =$con->prepare("SELECT * 
			                                FROM 
			                                    alkhutab
			                                WHERE 
			                                     id=? 
			                                   ");
			           $statement->execute(array($Edit));
			           $row=$statement->fetch();

			            $count=$statement->rowCount();

			            if($statement->rowCount() > 0){
		?>
       <h1 class="text-center">Edit New alkhutab</h1>
       <div class="container">
       <form method="POST" action="?do=Update" class="control" enctype="multipart/form-data">
       	<input type="hidden" name="Editid" value="<?php echo $row['id'] ?>">
       	<div class="form-group ">
       		    <label for="inputTitle" class="col-sm-2 control-label">Title</label>
       		      <div class="col-sm-10">
      <input type="text" class="form-control form-group" id="inputTitle" name="Title" value="<?php echo $row['title'] ?>">
    </div>
       	</div>
       	<div class="form-group ">
       		<label for="TEXTContent" class="col-sm-2 control-label">Content</label>
       		<div class="col-sm-10">
       			<textarea name="Content" class="form-control  form-group ckeditor" ><?php echo $row['content'] ; ?></textarea>
       		</div>
       	</div>
	
       	  	<div class="form-group">
       		    <label for="inputDate" class="col-sm-2 control-label">Date</label>
       		      <div class="col-sm-10">
      <input type="text" class="form-control  margin-top" id="inputDate" name="Date"value="<?php echo $row['date'] ?>">
    </div>
       	</div>
       	  	  	<div class="form-group text-center">
       		    <label  class="col-sm-2"></label>

   <input type="submit" class="btn btn-info margin-top" value="Save">

       	</div>
       </form>
		<?php
	}
	// if id edit not right
else{
 echo '<div class="container">';
        $mas='<div  class="alert alert-danger">You Edit id Not right </div> ';
        Redurict($mas,'jjj');
    }
}
	//End EDIT 

//start Update Information 
else if($do==="Update"){
		if($_SERVER['REQUEST_METHOD']=="POST")
				{

	           echo '<div class="container">';
	           echo '<h1 class="text-center"> Update  Page</h1>';
	           	$Title=$_POST['Title'];
				$Content=$_POST['Content'];
				$Date=$_POST['Date'];
				$editid=$_POST['Editid'];
	       
	            	$statement=$con->prepare("UPDATE alkhutab SET title=?,Content=?,Date=? WHERE id=?");
	            	$statement->execute(array($Title,$Content,$Date,$editid));
	            	echo '<div class="container">';
	                $message='<div  class="alert alert-success"> '.$statement->rowCount().' Inserted'.'</div>';
	                Redurict($message,'back');    
	             
	            

	     
}

}
//End Upate Information

//start delete
else if($do==='DELETE'){
	 $Edit=isset($_GET['Deleteid']) && is_numeric($_GET['Deleteid'])? intval($_GET['Deleteid']):0;
	   $checkItem=checkItem('id','alkhutab',$Edit);
           if($checkItem > 0)
           {  
           	      $statement=$con->prepare("SELECT * FROM alkhutab WHERE id=?");
                  $statement->execute(array($Edit));
                  $name=$statement->fetch();
                  $statement=$con->prepare("DELETE FROM alkhutab WHERE id=?");
                  $statement->execute(array($Edit));
               
                  if($statement->rowCount() > 0)
                  {
                   ob_start();
                     	echo '<div class="container">';
                   $mas='<div class="alert alert-success">'.$statement->rowCount() .' Recored deleted'.'</div>';
                   Redurict($mas,'back');
                   }
          }

echo '</div>';
}
//end Delete

include "includes/templats/footer.php";