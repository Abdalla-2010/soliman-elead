<?php 
   session_start();
// include init include to contect file and page navbar and style 
		    include 'init.php';
		    // page name
		  $pagetitle="translated_material";
		//get to parameter do in link
		$do=isset($_GET['do'])?$_GET['do']:'Manage';
		// start manage translated_material
				if($do === 'Manage')
		{
		?>
<!--  manage translated_material html-->
				<h1 class="text-center ">manage translated_material</h1> 
				   <div class="container">
				   	<!--button Add new translated material-->
				   	<a href="?do=ADD" class="btn btn-info ">ADD NEW translated material</a>

				   <table class="table table-responsive ">
				   	<thead>
				   		<tr>
				   			<th>Number Of translated material</th>
				   			<th>Arabic translated material</th>
				   			<th> date of  translated material</th>
				   			<th>Control</th>
				   		</tr>
				   		
				   	</thead>
<?php 
				$statement=$con->prepare("SELECT * FROM translated_material");
				$statement->execute();
				$rows=$statement->fetchAll();
				  echo "<tbody>";
				 foreach ($rows as $key => $value) {
				echo "<tr>";
				echo "<td>".$value['id']."</td>";
				echo "<td>".$value['title']."</td>";
				echo "<td>".$value['translated']."</td>";
 
 				echo "<td>".$value['date']."</td>";
				echo "<td><a  href='?do=EDIT&&Editid=".$value['id']."' class='btn btn-success'>EDIT</a>
				<a  href='?do=DELETE&&Deleteid=".$value['id']."' class='btn btn-danger'>DELETE</a>
				</td>";
                echo "</tr>";

				 }
?>
			</tbody>
			</table>

</div>
<?php		
		}
// show ADD New translated_material
		else if($do==="ADD")
		{
       ?>
       <h1 class="text-center">Add New translated material</h1>
       <div class="container">
       <form method="POST" action="?do=Insert" class="control" enctype="multipart/form-data">
       	<div class="form-group ">
       		    <label for="inputTitle" class="col-sm-2 control-label">Title</label>
       		      <div class="col-sm-10">
      <input type="text" class="form-control form-group" id="inputTitle" name="Title">
    </div>
       	</div>
       	<div class="form-group ">
       		<label for="arabic_translated" class="col-sm-2 control-label">arabic translated</label>
       		<div class="col-sm-10">
       			<textarea name="arabic_translated" class="form-control  form-group ckeditor" ></textarea>
       		</div>
       	</div>
 	
  
       	  	<div class="form-group ">
       		<label for="PDF" class="col-sm-2 control-label">PDF</label>
       		<div class="col-sm-10">
      <input type="file" class="form-control form-group" id="PDF" name="PDF">
       		</div>
       	</div>
       	  	<div class="form-group">
       		    <label for="inputDate" class="col-sm-2 control-label">Date</label>
       		      <div class="col-sm-10">
      <input type="text" class="form-control  margin-top" id="inputDate" name="Date">
    </div>
       	</div>
       	  	  	<div class="form-group text-center">
       		    <label  class="col-sm-2"></label>

   <input type="submit" class="btn btn-info margin-top" value="Save">

       	</div>
       </form>
       </div>
       <?php

		}

		// Insert New translated_material
		else if ($do==="Insert")
		{
			// check  if method post
			if($_SERVER['REQUEST_METHOD']=="POST")
			{

           echo '<div class="container">';
           echo '<h1 class="text-center"> Insert Page</h1>';

				$Title=$_POST['Title'];
				$arabic_translated=$_POST['arabic_translated'];
				$Date=$_POST['Date'];
				//start pdf information
                

			// if (isset($_FILES['PDF']) && $_FILES['PDF']['error'] === UPLOAD_ERR_OK)
			//   {
			    // get details of the uploaded file PDF 

			    $fileTmpPathPDF = $_FILES['PDF']['tmp_name'];
			    $fileNamePDF = $_FILES['PDF']['name'];
			    $fileSizePDF = $_FILES['PDF']['size'];
			    $fileTypePDF = $_FILES['PDF']['type'];
			    $fileNameCmpsPDF = explode(".", $fileNamePDF);
			    $fileExtensionPDF = strtolower(end($fileNameCmpsPDF));
			
			 //PDF  sanitize file-name
			  $newFileNamePDF = md5(time() . $fileNamePDF) . '.' . $fileExtensionPDF;

			   			 //PDF check if file has one of the following extensions 
			    $allowedfileExtensionsPDF = array('pdf');

			    // if (in_array($fileExtensionPDF, $allowedfileExtensionsPDF))
			    // {
			  
	         // pdf directory in which the uploaded file will be moved
                  $uploadFileDirPDF = 'pdf/translated';
			      $dest_pathPDF = $uploadFileDirPDF . $newFileNamePDF;
			      // if(move_uploaded_file($fileTmpPathPDF, $dest_pathPDF)) 
			      // {
						//use function to check name if exiset
			    
			
					//		PDF MOVE
					move_uploaded_file($fileTmpPathPDF, $dest_pathPDF);

					//insert Information table  translated_material

					$statement=$con->prepare("INSERT INTO translated_material(title,translated,date,file)
						VALUES(:mtitle,:mtranslated,:mdate,:mfile)");
					$statement->execute( array(
						'mtitle'         => $Title,
						'mtranslated'    => $arabic_translated,
						'mdate'          => $Date,
						'mfile'          => $newFileNamePDF
					 ));
	   $message='<div  class="alert alert-success"> '.$statement->rowCount().' Inserted'.'</div>';
	    Redurict($message,'back');     
			// 	}

			// }
			// 	      }
			//    else
			//       {
			//         $message = 'There was some error moving the file to upload directory. Please make sure the upload directory is writable by web server.';
			  
			//   		    Redurict($message,'back');     
   //  }
			//     }
			//   else
			//     {
			//       $message = 'Upload failed. Allowed file types: '.implode(',', $allowedfileExtensionsPDF) ;
			//    		    Redurict($message,'back');     

			//     }
			//   }
			//   else
			//   {
			//     $message = 'There is some error in the file upload. Please check the following error.<br>';
			//     $message .= 'Error:' . $_FILES['PDF']['error'];
			//     		    Redurict($message,'back');     

			//   }

			// 	//End Image Information 
			
			// 	}else{
			// 	// if method not POST
		 //    echo '<div class="container">';
		 //        $mas='<div  class="alert alert-danger">You Not Allow To Come Here </div> ';
		 //        Redurict($mas,'jjj');
			// 	}
			} 
	//End Insert 
}
	//start EDIT
	else if($do=="EDIT"){
			 $Edit=isset($_GET['Editid']) && is_numeric($_GET['Editid'])? intval($_GET['Editid']):0;
			           $statement =$con->prepare("SELECT * 
			                                FROM 
			                                    translated_material
			                                WHERE 
			                                     id=? 
			                                   ");
			           $statement->execute(array($Edit));
			           $row=$statement->fetch();

			            $count=$statement->rowCount();

			            if($statement->rowCount() > 0){
		?>
       <h1 class="text-center">Edit New translated material</h1>
       <div class="container">
       <form method="POST" action="?do=Update" class="control" enctype="multipart/form-data">
       	<input type="hidden" name="Editid" value="<?php echo $row['id'] ?>">
       	<div class="form-group ">
       		    <label for="inputTitle" class="col-sm-2 control-label">Title</label>
       		      <div class="col-sm-10">
      <input type="text" class="form-control form-group" id="inputTitle" name="Title" value="<?php echo $row['title'] ?>">
    </div>
       	</div>
       

       		<div class="form-group ">
       		<label for="arabic_translated" class="col-sm-2 control-label">arabic translated</label>
       		<div class="col-sm-10">
       			<textarea name="arabic_translated" class="form-control  form-group ckeditor" ><?php echo $row['translated'] ; ?></textarea>
       		</div>
       	</div>

       <div class="form-group ">
       		<label for="PDF" class="col-sm-2 control-label">PDF</label>
       		<div class="col-sm-10">
      <input type="file" class="form-control form-group" id="PDF" name="PDF">
       		</div>
       	</div>
       
       	  	<div class="form-group">
       		    <label for="inputDate" class="col-sm-2 control-label">Date</label>
       		      <div class="col-sm-10">
      <input type="text" class="form-control  margin-top" id="inputDate" name="Date"value="<?php echo $row['date'] ?>">
    </div>
       	</div>
       	  	  	<div class="form-group text-center">
       		    <label  class="col-sm-2"></label>

   <input type="submit" class="btn btn-info margin-top" value="Save">

       	</div>
       </form>
		<?php
	}
	// if id edit not right
else{
 echo '<div class="container">';
        $mas='<div  class="alert alert-danger">You Edit id Not right </div> ';
        Redurict($mas,'jjj');
    }
}
	//End EDIT 

//start Update Information 
else if($do==="Update"){
		if($_SERVER['REQUEST_METHOD']=="POST")
				{

	           echo '<div class="container">';
	           echo '<h1 class="text-center"> Update  Page</h1>';
	          	$Title=$_POST['Title'];
				$arabic_translated=$_POST['arabic_translated'];
				$editid=$_POST['Editid'];
				$Date=$_POST['Date'];
	            $PDF=$_FILES['PDF']['name'];

	         


	            if( $PDF==''){




              
	            	$statement=$con->prepare("UPDATE translated_material SET title=?,translated=?,un_translated=?,Date=? WHERE id=?");
	            	$statement->execute(array($Title,$arabic_translated,$english_translated,$Date,$editid));
	            	echo '<div class="container">';
	                $message='<div  class="alert alert-success"> '.$statement->rowCount().' Inserted'.'</div>';
	                Redurict($message,'back');    
	        
	            }else{
// if image not exiset

			   
			    //PDF INFOMRATION
			    $fileTmpPathPDF = $_FILES['PDF']['tmp_name'];
			    $fileNamePDF = $_FILES['PDF']['name'];
			    $fileSizePDF = $_FILES['PDF']['size'];
			    $fileTypePDF = $_FILES['PDF']['type'];
			    $fileNameCmpsPDF = explode(".", $fileNamePDF);
			    $fileExtensionPDF = strtolower(end($fileNameCmpsPDF));
			   
			  //PDF  sanitize file-name
			   $newFileNamePDF = md5(time() . $fileNamePDF) . '.' . $fileExtensionPDF;
 
		
						 //PDF check if file has one of the following extensions 
			    $allowedfileExtensionsPDF = array('pdf');

			    if (in_array($fileExtensionPDF, $allowedfileExtensionsPDF))
			    {
			      // directory in which the uploaded file will be moved
			  
		 $uploadFileDirPDF = 'pdf/translated';
			      $dest_pathPDF = $uploadFileDir . $newFileNamePDF;
			      if(move_uploaded_file($fileTmpPathPDF, $dest_pathPDF)) 
			      {
					//insert Information table  translated_material

{
    					move_uploaded_file($fileTmpPathPDF, $dest_pathPDF);

					$statement=$con->prepare("UPDATE translated_material SET title=?,translated=?,un_translated=?,Date=?,img=?,file=? WHERE id=?	");
	            	$statement->execute(array($Title,$arabic_translated,$english_translated,$Date,$newFileNamePDF,$editid));
	            	echo '<div class="container">';
	         $message='<div  class="alert alert-success"> '.$statement->rowCount().' Inserted'.'</div>';
	    Redurict($message,'back');     
				
				      }
				  }

				      else
				      {
				        $message = 'There was some error moving the file to upload directory. Please make sure the upload directory is writable by web server.';
				    Redurict($message,'back');     

				      }
	    }
	                else
                     {
		    $message = 'Upload failed. Allowed file types: ' .implode(',', $allowedfileExtensionsPDF);
              	    Redurict($message,'back');     

                     }
            	} 
            

}

}
//End Upate Information

//start delete
else if($do==='DELETE'){
	 $Edit=isset($_GET['Deleteid']) && is_numeric($_GET['Deleteid'])? intval($_GET['Deleteid']):0;
	   $checkItem=checkItem('id','translated_material',$Edit);
           if($checkItem > 0)
           {  
           	      $statement=$con->prepare("SELECT * FROM translated_material WHERE id=?");
                  $statement->execute(array($Edit));
                  $name=$statement->fetch();
                  $statement=$con->prepare("DELETE FROM translated_material WHERE id=?");
                  $statement->execute(array($Edit));
                     $filenamePDF="PDF/".$name['file'];
				  if (file_exists($filenamePDF))
				  {
				  	         unlink($filenamePDF);
				  }
		
                  if($statement->rowCount() > 0)
                  {
                   ob_start();
                     	echo '<div class="container">';
                   $mas='<div class="alert alert-success">'.$statement->rowCount() .' Recored deleted'.'</div>';
                   Redurict($mas,'back');
                   }
          }

echo '</div>';
}
//end Delete

include "includes/templats/footer.php";