
<html lang="ar">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>solaimanalead</title>
    <link rel="icon" href="img/Untitled-1.jpg" type="image/icon type">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@300;400;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>

    <div class="page-container">

        <div class="notfooter">
            <div class="page-header">
                <div class="banner">
                    <div class="banner-header col-12">

                        <div class="col-6">
                        
                        </div>

                        <div class="col-6">
                            <div class="search">
                                <form method="GET" action="sreach.php" class="d-flex">
                                <input type="search" name="searchtxt" id="searchtxt" />
                                <input type="submit" id="searchbtn" id="searchbtn" value="بحث" />
                           
                                </form>
 
                            </div>
                        </div>

                    </div>
                    <div class="banner-logo col-12">
                        <img src="img/logo.png" alt="Logo" class="logo">
                    </div>
                    <div class="banner-navigation col-7 m-auto">
                        <ul class="nav nav-pills nav-fill justify-content-center">
                            <li class="nav-item">
                                <a class="nav-link " href="index.php">الرئيسية</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link " aria-current="page" href="AboutUs.php">السيرة الذاتية</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="lectuer.php">الدروس والمحاضرات</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="allbook.php">الكتب</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="resreach.php">الأبحاث</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="alkhutab.php">الخطب</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="articles.php">المقالات</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="allsound.php">الصوتيات</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="translated_material.php">مواد مترجمة</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="consulting.php">الاستشارات</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="tweets.php">تغريدات</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#0">روابط مهمة</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#0">اتصل بنا</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
