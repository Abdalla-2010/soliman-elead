
        <footer class="page-footer">
            <div class="layer row">
                <div class="col-3 logo-contact">
                    <div class="logo">
                        <img src="img/logo.png" alt="Logo">
                    </div>
                    <div class="social-media d-flex justify-content-center">
                        <a href="#0">
                            <i class="fas fa-share-alt"></i>
                        </a>
                        <a href="https://twitter.com/su1418">
                            <i class="fab fa-twitter"></i>
                        </a>
                        <a href="#0">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                        <a href="#0">
                            <i class="fab fa-google-plus-g"></i>
                        </a>
                        <a href="#0">
                            <i class="fab fa-whatsapp"></i>
                        </a>
                    </div>
                </div>
                <div class="col-6 links-container">
                    <div class="links">
                        <nav>
                            <ul>
                                <li><a href="tweets.php">تغريدات</a></li>
                                <li><a href="translated_material.php">مواد مترجمة</a></li>
                                <li><a href="#0">اتصل بنا</a></li>
                            </ul>

                            <ul>
                                <li><a href="alkhutab.php">الخطب</a></li>
                                <li><a href="allbook.php">الكتب</a></li>
                                <li><a href="articles.php">المقالات</a></li>
                                <li><a href="allsound.php">الصوتيات</a></li>
                                <li><a href="consulting.php">الاستشارات</a></li>
                            </ul>

                            <ul>
                                <li><a href="index.php">الرئيسية</a></li>
                                <li><a href="AboutUs.php">السيرة الذاتية</a></li>
                                <li><a href="">روابط مهمة</a></li>
                                <li><a href="resreach.php">الأبحاث</a></li>
                                <li><a href="lectuer.php">الدروس والمحاضرات</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-3 copyrights-container">
                    <div class="copyrights">
                      <a href="http://sharkegypt.com">  <h4>
                            2021
                        </h4>
                        <h4>
                            جميع الحقوق محفوظة
                        </h4>
                        <h4>
                            تطوير وتنفيذ شركة شارك
                        </h4>
                  </a>
                    </div>
                </div>
            </div>
        </footer>


    <!-- Jquery -->
    <script src="js/jquery.js"></script>
<script
  src="https://code.jquery.com/jquery-3.6.0.js"
  integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
  crossorigin="anonymous"></script>
    <!-- bootstrap -->
    <script src="js/bootstrap.min.js"></script>

    <!-- swiperjs -->
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

    <!-- script -->
    <script src="js/script.js"></script>

<script type="text/javascript">
    const swiper = new Swiper('.swiper-container', {
  autoplay: {
    delay: 2000,
  },
});


</script>
</body>

</html>